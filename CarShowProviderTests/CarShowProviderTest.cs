﻿using System;
using CarShowService;
using CarShowService.Providers;
using CarShowService.Settings;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace CarShowProviderTests
{
    public class CarShowProviderTest
    {
        [Fact]
        public void Give_ValidJsonData_When_NotSorted_Then_ReturnsSorted()
        {
            var httpMock = Substitute.For<IHttpProvider>();
            var settingsMock = Substitute.For<ISettingsProvider>();
            var loggerMock = Substitute.For<ILoggerFactory>();

            var carshowProvider = new CarShowProvider(httpMock, settingsMock, loggerMock);
        }
    }
}
