﻿namespace CarShowService.Settings
{
    public class SettingsProvider : ISettingsProvider
    {
        private static string carShowEndpoint = "http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars";

        public string GetCarShowEndpoint()
        {
            // Todo: read from config
            return carShowEndpoint;
        }
    }
}
