﻿namespace CarShowService.Settings
{
    public interface ISettingsProvider
    {
        string GetCarShowEndpoint();
    }
}
