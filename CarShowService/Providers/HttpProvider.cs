﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CarShowService.Providers
{
    public class HttpProvider : IHttpProvider
    {
        public static HttpClient HttpClient;

        public HttpProvider()
        {
            HttpClient = new HttpClient();
        }

        public async Task<string> Get(string url)
        {
            var response = await HttpClient.GetAsync(url);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }
    }
}
