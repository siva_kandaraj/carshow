﻿using System.Collections.Generic;
using CarShowService.Models;

namespace CarShowService.Providers
{
    public interface ICarShowProvider
    {
        IEnumerable<GroupedResult> GetCarShows();
    }
}
