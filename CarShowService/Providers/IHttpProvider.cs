﻿using System.Threading.Tasks;

namespace CarShowService.Providers
{
    public interface IHttpProvider
    {
        Task<string> Get(string url);
    }
}
