﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarShowService.Models;
using CarShowService.Settings;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static System.String;

namespace CarShowService.Providers
{
    public class CarShowProvider : ICarShowProvider
    {
        private readonly IHttpProvider _httpProvider;
        private readonly string _url;
        private readonly ILogger<ICarShowProvider> _logger;

        public CarShowProvider(
            IHttpProvider httpProvider,
            ISettingsProvider settingsProvider,
            ILoggerFactory loggerFactory)
        {
            _httpProvider = httpProvider;
            _url = settingsProvider.GetCarShowEndpoint();
            _logger = loggerFactory.CreateLogger<ICarShowProvider>();
        }

        public IEnumerable<GroupedResult> GetCarShows()
        {
            string response;
            // TODO: extend response error and return proper messages to UI
            try
            {
                response = _httpProvider.Get(_url).Result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }

            return MapAndSortToCarShow(response);
        }

        private static IEnumerable<GroupedResult> MapAndSortToCarShow(string response)
        {
            // TODO: Assumption is to always expect 200 and right JSON. Could be better with error handling.
            var mappedCarShows = JsonConvert.DeserializeObject<IEnumerable<CarShow>>(response);

            return mappedCarShows?.SelectMany(show => show.Cars.GroupBy(_ => _.Make)
                .Select(car => new GroupedResult
                {
                    Make = car.FirstOrDefault()?.Make,
                    Model = IsNullOrWhiteSpace(car.First().Model) ? "No model." : car.First().Model,
                    ShowName = IsNullOrWhiteSpace(show.Name) ? "No show name." : show.Name
                })).OrderBy(o => o.Make);
        }
    }
}
