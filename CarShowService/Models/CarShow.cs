﻿using System.Collections.Generic;

namespace CarShowService.Models
{
    public class CarShow
    {
        public string Name { get; set; }
        public List<Car> Cars { get; set; }
    }
}
