﻿using System.Collections.Generic;

namespace CarShowService.Models
{
    public class GroupedResult
    {
        public string ShowName;
        public string Model;
        public string Make;
    }
}
