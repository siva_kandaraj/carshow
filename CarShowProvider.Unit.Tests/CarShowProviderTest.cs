﻿using System.Linq;
using CarShowService.Providers;
using CarShowService.Settings;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace CarShowProvider.Unit.Tests
{
    public class CarShowProviderTest
    {
        private readonly IHttpProvider _httpMock;
        private readonly ISettingsProvider _settingsMock;
        private readonly ILoggerFactory _loggerMock;

        public CarShowProviderTest()
        {
            _httpMock = Substitute.For<IHttpProvider>();
            _settingsMock = Substitute.For<ISettingsProvider>();
            _loggerMock = Substitute.For<ILoggerFactory>();
        }

        [Fact]
        public void Give_ValidJsonData_When_NotSorted_Then_ReturnsSorted()
        {
            // Todo: Move test data to a file
            _httpMock.Get(Arg.Any<string>()).Returns(
                "[{\"name\":\"Carographic\",\"cars\":[{\"make\":\"Hondaka\",\"model\":\"Elisa\"},{\"make\":\"Hondaka\",\"model\":\"Elisa\"},{\"make\":\"Julio Mechannica\",\"model\":\"Mark 4\"}]}]");

            var carshowProvider = new CarShowService.Providers.CarShowProvider(_httpMock, _settingsMock, _loggerMock);

            var carshows = carshowProvider.GetCarShows();

            carshows.Count().Should().Be(2);

            // Todo: check assertion for sorting and unique data
        }

        [Fact]
        public void Give_ValidMultipleJsonData_When_NotSorted_Then_ReturnsSorted()
        {
            // Todo: Move test data to a file
            _httpMock.Get(Arg.Any<string>()).Returns(
                "[\n    {\n        \"name\": \"New York Car Show\",\n        \"cars\": [\n            {\n                \"make\": \"Hondaka\",\n                \"model\": \"Elisa\"\n            },\n            {\n                \"make\": \"George Motors\",\n                \"model\": \"George 15\"\n            },\n            {\n                \"make\": \"Julio Mechannica\",\n                \"model\": \"Mark 1\"\n            },\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Cyclissimo\"\n            },\n            {\n                \"make\": \"Edison Motors\",\n                \"model\": \"\"\n            }\n        ]\n    },\n    {\n        \"name\": \"Melbourne Motor Show\",\n        \"cars\": [\n            {\n                \"make\": \"Julio Mechannica\",\n                \"model\": \"Mark 4S\"\n            },\n            {\n                \"make\": \"Hondaka\",\n                \"model\": \"Elisa\"\n            },\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Cyclissimo\"\n            },\n            {\n                \"make\": \"George Motors\",\n                \"model\": \"George 15\"\n            },\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Delta 4\"\n            }\n        ]\n    },\n    {\n        \"name\": \"Cartopia\",\n        \"cars\": [\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Cyclissimo\"\n            },\n            {\n                \"make\": \"George Motors\",\n                \"model\": \"George 15\"\n            },\n            {\n                \"make\": \"Hondaka\",\n                \"model\": \"Ellen\"\n            },\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Delta 16\"\n            },\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Delta 4\"\n            },\n            {\n                \"make\": \"Julio Mechannica\",\n                \"model\": \"Mark 2\"\n            }\n        ]\n    },\n    {\n        \"name\": \"Carographic\",\n        \"cars\": [\n            {\n                \"make\": \"Hondaka\",\n                \"model\": \"Elisa\"\n            },\n            {\n                \"make\": \"Hondaka\",\n                \"model\": \"Elisa\"\n            },\n            {\n                \"make\": \"Julio Mechannica\",\n                \"model\": \"Mark 4\"\n            },\n            {\n                \"make\": \"Julio Mechannica\",\n                \"model\": \"Mark 2\"\n            },\n            {\n                \"make\": \"Moto Tourismo\"\n            },\n            {\n                \"make\": \"Julio Mechannica\",\n                \"model\": \"Mark 4\"\n            }\n        ]\n    },\n    {\n        \"cars\": [\n            {\n                \"make\": \"Moto Tourismo\",\n                \"model\": \"Delta 4\"\n            }\n        ]\n    }\n]");

            var carshowProvider = new CarShowService.Providers.CarShowProvider(_httpMock, _settingsMock, _loggerMock);

            var carshows = carshowProvider.GetCarShows();

            carshows.Count().Should().Be(17);

            // Todo: better assertion
        }
    }
}
