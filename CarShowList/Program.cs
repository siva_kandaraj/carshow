﻿using System;
using CarShowService;
using CarShowService.Providers;
using CarShowService.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CarShowUi
{
    internal class Program
    {
        private static void Main()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<IHttpProvider, HttpProvider>()
                .AddSingleton<ICarShowProvider, CarShowProvider>()
                .AddSingleton<ISettingsProvider, SettingsProvider>()
                .BuildServiceProvider();

            //configure console logging
            serviceProvider
                .GetService<ILoggerFactory>()
                .AddConsole(LogLevel.Debug);

            var logger = serviceProvider.GetService<ILoggerFactory>()
                .CreateLogger<Program>();

            logger.LogDebug("Starting carshow application");

            var bar = serviceProvider.GetService<ICarShowProvider>();

            while (true)
            {
                var groupedResults = bar.GetCarShows();

                if (groupedResults == null)
                {
                    Console.WriteLine("Woops, something went wrong.");
                    Console.Read();
                    continue;
                }

                foreach (var result in groupedResults)
                {
                    Console.WriteLine($"{result.Make}\n\t{result.Model}\n\t\t{result.ShowName}");
                }

                Console.WriteLine("Enter to continue...");
                Console.ReadLine();
            }
        }
    }
}
